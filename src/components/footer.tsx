
export default function Footer (){
    return(
        <footer>
            <span>Copyright &copy; <b>Undelete plus</b></span>
        </footer>
    )
}
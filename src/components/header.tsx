import Link from "next/link";

export default function Header (){
  return (
    <header>
      <Link href='/'>
        <img src="/vercel.svg" alt="logo" width='100' height='auto'/>
      </Link>
      <nav>
        <ul>
          <li><Link href='/products'>Products</Link></li>
          <li><Link href='/help'>How to use</Link></li>
          <li><Link href='/routes'>routes</Link></li>
          <li><Link href='/login'>Login</Link></li>
        </ul>
      </nav>
    </header>
  )
}
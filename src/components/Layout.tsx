import Head from 'next/head'
import {ReactNode} from "react";

type ProfileProps = {
  title?: string
  description?: string
  keywords?: string
}

export default function Layout({
  title = 'recovery app',
  description = 'recovery app',
  keywords = 'recovery app'
}: ProfileProps) {
  return (
      <>
        <Head>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta name="keywords" content={keywords} />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
      </>
  )
}

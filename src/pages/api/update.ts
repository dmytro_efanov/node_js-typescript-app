import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method, body } = req

  switch (method) {
    case 'PUT':
      try {
        const data = JSON.parse(body)

        // Add backend logic
        res.status(200).json({
          message: `Success the input is = ${data.id}`,
        })
        return
      } catch (err: any) {
        // Catch for axios
        res.status(err.response.status).json(err.response.data)
        return
      }
    default:
      res.setHeader('Allow', ['PUT'])
      res.status(405).end(`Method ${method} not allowed`)
  }
}

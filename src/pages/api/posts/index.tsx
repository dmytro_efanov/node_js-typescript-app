import { NextApiRequest, NextApiResponse } from 'next'

export default function (req:NextApiRequest, res:NextApiResponse){

  if(req.method === 'GET'){
    res.status(200).json({name: 'John Doe'})
  } else{
    res.setHeader('Allow', ['GET'])
    res.status(405).json({message: `Method ${req.method} is not allowed`})
  }

}
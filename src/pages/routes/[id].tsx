import {useRouter} from "next/router";

interface IProps {
  id: string;
}

export default function Route(id: IProps) {
  const router = useRouter()
  console.log(router)
  const handleUpdate = async () => {
    const response = await fetch('/api/posts/update', {
      method: 'PUT',
      body: JSON.stringify({
        id,
      }),
    })

    const data = response.json()

    console.log(data)
  }
  return (
    <div>
      <>query of this page was {router.query.id} </>

      <button type="button" onClick={handleUpdate}>
        Submit
      </button>
    </div>
  )
}

export async function getStaticProps() {
  return {
    props: { id: 1, revalidate: 40 },
  }
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { id: '1' } }, { params: { id: '2' } }],
    fallback: false, // can also be true or 'blocking'
  }
}

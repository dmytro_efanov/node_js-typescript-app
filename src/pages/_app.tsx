import '@/src/styles/globals.css'
import type {AppProps} from 'next/app'
import {useRouter} from "next/router";
import Layout from "@/src/components/Layout";
import Header from '@/src/components/header'
import Footer from '@/src/components/footer'
import Showcase from "@/src/pages/showcase";


export default function App ({Component, pageProps}: AppProps){
  const router = useRouter()
  return (
    <>
      <Layout/>
      <Header/>
      <div className='container'>
        {router.pathname === '/' && <Showcase/>}
        <Component {...pageProps} />
      </div>
      <Footer/>
    </>

  )
}


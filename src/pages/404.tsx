import styles from '@/src/styles/404.module.css'

export default function NotFoundPage (){
  return(
    <div className={styles.errorPage}>
      <section className={styles.err}>
        <h1>404</h1>
        <h4>Page not found</h4>
      </section>

    </div>
  )
}